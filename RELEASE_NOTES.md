Release Notes
-------------
2.0.3
-----
* upsert is now using DBOptions.

2.0.2
-----
* Added missing "use" dependency for DBOptions.

2.0.1
-----
* Now using DBOptions type.

2.0.0
-----
* [BC Break] Updated to add new methods from CommonQueryInterface: buildColumn, collectionExists, columnExists.

1.0.1
-----
* Added copyright header.

1.0.0
-----
* Initial Release.

